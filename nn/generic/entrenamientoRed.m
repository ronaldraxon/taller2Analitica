function performance = entrenamientoRed( entradas,salidas,entrenamiento, cantidadNeuronasOcultas,prueba,mejorPerf,variables)
%ENTRENAMIENTORED Summary of this function goes here
%   Detailed explanation goes here
x = entradas';
t = salidas';

% Choose a Training Function
% For a list of all training functions type: help nntrain
% 'trainlm' is usually fastest.
% 'trainbr' takes longer but may be better for challenging problems.
% 'trainscg' uses less memory. Suitable in low memory situations.
%trainFcn = entrenamiento;  % Scaled conjugate gradient backpropagation.

%tamanio = size(x);
hiddenLayerSize = cantidadNeuronasOcultas;
net =feedforwardnet(hiddenLayerSize,entrenamiento);
net = configure(net,x,t);
%net.numinputs = tamanio(2);
% Create a Pattern Recognition Network
%net = patternnet(hiddenLayerSize);
%net.trainFcn = entrenamiento;
% Choose Input and Output Pre/Post-Processing Functions
% For a list of all processing functions type: help nnprocess
net.input.processFcns = {'removeconstantrows','mapminmax'};
net.output.processFcns = {'removeconstantrows','mapminmax'};

% Setup Division of Data for Training, Validation, Testing
% For a list of all data division functions type: help nndivide
net.divideFcn = 'dividerand';  % Divide data randomly
net.divideMode = 'sample';  % Divide up every sample
net.divideParam.trainRatio = 90/100;
net.divideParam.valRatio = 1/100;
net.divideParam.testRatio = 10/100;

% Choose a Performance Function
% For a list of all performance functions type: help nnperformance
%net.performFcn = 'crossentropy';  % Cross-Entropy
net.performFcn = 'mse';  % Mean squared error performance function.

% Choose Plot Functions
% For a list of all plot functions type: help nnplot
%net.plotFcns = {'plotperform','plottrainstate','ploterrhist', ...
% 'plotregression'};

% Train the Network
%[net,tr] = train(net,x,t,'useParallel','yes');
%[net,tr] = train(net,x,t,'useGPU','yes'); no soporta jacobiano en gpu
%view(net)
[ net,tr ] = train(net,x,t,'useParallel','yes');

testPerformance = sqrt(tr.best_tperf);
disp(num2str(testPerformance,'%.0f'))
%y = sim(net,x','useParallel','yes');
%Cyt = corrcoef(y,t);
%R  = Cyt(2,1);
if testPerformance<mejorPerf
disp("Nuevo Record: "+num2str(sqrt(tr.best_tperf),'%.2f')+ "con "+ cantidadNeuronasOcultas+ " neuronas del archivo " +variables);
mejorPerf = testPerformance;
s = strcat(num2str(testPerformance,'%.0f'),"_",num2str(cantidadNeuronasOcultas,'%.0f'),variables,".xlsx");
d = "W:\Shared_raxonserver\workAndStudy\magister\subjects\M�todos y aplicaciones de anal�tica I (Analytics I)\taller2Analitica\nnbestResults\";
filename = strcat(d,s);
z = sim(net,prueba','useParallel','yes');
xlswrite(filename,z')
end

performance = mejorPerf;
end